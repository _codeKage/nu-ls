import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, LoadingController, ActionSheetController } from 'ionic-angular';
import {BarcodeScanner, BarcodeScannerOptions} from "@ionic-native/barcode-scanner";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {Storage} from "@ionic/storage";
import * as Tesseract from 'tesseract.js'
import { HttpClient } from '@angular/common/http';
import { AddTopicPage } from '../add-topic/add-topic';
/**
 * Generated class for the AdminBookInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-book-info',
  templateUrl: 'admin-book-info.html',
})
export class AdminBookInfoPage {
  options:BarcodeScannerOptions;
  barcode:string;
  author:string;
  edition:string;
  title:string;
  constructor(private loading:LoadingController,private storage:Storage,private http:HttpClient,private camera:Camera,public navCtrl: NavController, public navParams: NavParams,private alertCtrl:AlertController,private BScanner:BarcodeScanner, public actionSheet: ActionSheetController) {
    this.barcode = this.navParams.get('barcode');
    this.author = this.navParams.get('author');
    this.title = this.navParams.get('book_title');
    this.edition = this.navParams.get('edition');
  }
  cameraOptions:CameraOptions={
    quality: 80,
    destinationType: this.camera.DestinationType.FILE_URI,
    sourceType: 1,
    encodingType: this.camera.EncodingType.JPEG,
    allowEdit: false,
    saveToPhotoAlbum: false,
    correctOrientation: true,
    mediaType: this.camera.MediaType.PICTURE
  };
  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminBookInfoPage');
  }

  showActionSheet() : void {
    let actionSheet = this.actionSheet.create({
      title: 'Image Source',
      buttons: [
        {
          text: 'Camera',
          handler: () => this.openCamera(1)
        },
        {
          text: 'Storage',
          handler: () => this.openCamera(0)
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    }).present();
  }

  lendBook(barcode:string,id_number:string):void{
    let load = this.loading.create({
      content:'Please Wait'
    });
    load.present();
    this.storage.get('id').then(data=>{
      let x:string = data;
      let postData = new FormData();
      postData.append('barcode',this.barcode);
      postData.append('lender_id',x);
      postData.append('id_num',id_number);
      this.http.post("http://nuls.x10host.com/librarian_lender.php",postData).subscribe((res : any)=>{
          load.dismissAll();
          if(res=='0'){
            this.showMessage('Book unavailable, may be recorded as missing or already borrowed');
          }
          else if(res=='1'){
            this.showMessage('ID number does not exist');
          }
          else if(res=='2'){
            this.showMessage('Book Lent');
          }

      },error2 => {
        this.showMessage('Internet Connection error');
        load.dismissAll();
      });
    },error=>{
      load.dismissAll();
      this.showMessage('Internal Error');
    });
  }
  showMessage(message:string): void {
    this.alertCtrl.create({
      title:'Message',
      subTitle:message,
      buttons:['ok']
    }).present();
  }
  returnBook(barcode:string): void {
    let load = this.loading.create({
      content:'Please Wait'
    });
    load.present();
    this.storage.get('id').then(data=>{
      let x = data;
      let postData = new FormData();
      postData.append('barcode',barcode);
      postData.append('receiver_id',x);
      this.http.post("http://nuls.x10host.com/return_book.php",postData).subscribe((data : any)=>{
        if(data=='1'){
          this.showMessage('Book Received');
          load.dismissAll();
        }
      },error=>{
        load.dismissAll();
        this.showMessage('Internet Conenction error');
      })
    },error=>{
      load.dismissAll();
      this.showMessage('Internal Problem');
    });
  }
  showCirculation():void{
    let alert = this.alertCtrl.create({
      title:'Student Number',
      inputs:[{
        name:'idNum',
        placeholder:'ID Number',
        type:'tel'
      }],
      buttons:[{
        text:'Lend',
        handler:data=>{
          this.lendBook(this.barcode,data.idNum.toString());
        }
      },
        {
          text:'Return',
          handler:data=>{
            this.returnBook(this.barcode);
          }
        },{
          text:'Cancel',
          role:'cancel',
          handler:()=>{

          }
        }]
    });
    alert.present();
  }

  showLend():void{
    let alert = this.alertCtrl.create({
      title:'Remarks',
      subTitle:'Book Lent',
      buttons:['ok']
    });
    alert.present();
  }
  showReturn():void{
    let alert = this.alertCtrl.create({
      title:'Remarks',
      subTitle:'Book Returned',
      buttons:['ok']
    });
    alert.present();
  }
  async openScanner(){
    this.options = {
      prompt:"Scan Barcode"
    };
    this.BScanner.scan(this.options).then(barcodeData=>{
      let alert = this.alertCtrl.create({
        title:'Barcode Data',
        subTitle:barcodeData.text,
        buttons:['Ok']
      });
      alert.present();
      this.navCtrl.push(AdminBookInfoPage);
    });
  }
  updateRemarks(remark:string):void{
    let postData = new FormData();
    postData.append('barcode',this.barcode);
    postData.append('remark',remark);
    this.http.post("http://nuls.x10host.com/update_remarks.php",postData).subscribe((data: any)=>{
      if(data=='1'){
        this.showMessage('Remark Updated');
      }
    },error=>{
      this.showMessage('Internet Connection error');
    })
  }
  showRemarks(): void
  {
    let prompt = this.alertCtrl.create({
      title: 'Book Remarks',
      message: 'Select Remarks ',
      inputs : [
        {
          type:'radio',
          label:'Dilapidated',
          value:'Dilapidated',
          name:'remark',
        },
        {
          type:'radio',
          label:'For Covering',
          value:'For Covering',
          name:'remark'
        },
        {
          type:'radio',
          label:'Weed Out',
          value:'Weed Out',
          name:'remark'
        },
        {
          type:'radio',
          label:'Missing Page',
          value:'Missing page',
          name:'remark'
        },
        {
          type:'radio',
          label:'Deselected',
          value:'Deselected',
          name:'remark'
        },
        {
          type: 'radio',
          label: 'Remove Remarks',
          value:'none',
          name: 'remark'
        }
        ],
      buttons : [
        {
          text: "Cancel",
          handler: data => {
            console.log("cancel clicked");
          }
        },
        {
          text: "Ok",
          handler: data => {
           this.updateRemarks(data.toString());
          }
        }]});
    prompt.present();
  }
  openCamera(source: number) {
    this.cameraOptions.sourceType = source;
    // this.cameraOptions.allowEdit = source === 0;
    this.cameraOptions.allowEdit = true;
    this.camera.getPicture(this.cameraOptions).then((imageData => {
      let load = this.loading.create({
        content: 'Please wait, recognizing image'
      });
      load.present();
      Tesseract.recognize(imageData).progress((prog) => {
      }).then(data=>{
        this.navCtrl.push(AddTopicPage, {
          topics: data.text,
          barcode: this.barcode
        });
        load.dismiss();
        // this.alertCtrl.create({
        //   title:'Topics',
        //   subTitle:`<ion-textarea>${ data.text }</ion-textarea>`,
        //   buttons:[{
        //     text:'Save',
        //     handler:()=>{
        //       load.dismissAll();
        //       let load2 = this.loading.create({
        //         content: 'Recognizing Text'
        //       });
        //       load2.present();
        //
        //       let postData = new FormData();
        //       postData.append('barcode', this.barcode);
        //       postData.append('topic', data.text);
        //       // this.http.post("http://nuls.x10host.com/add_topic.php", postData).subscribe((res : any) => {
        //       //   if (res == '1') {
        //       //     load2.dismissAll();
        //       //     this.showMessage('Adding Topics Success');
        //       //   } else if (res == '0') {
        //       //     load2.dismissAll();
        //       //     this.showMessage('Adding Topics Failed, too many characters');
        //       //   }
        //       //   load.dismissAll();
        //       // }, err => {
        //       //   this.showMessage('Internet Connection Problem');
        //       //   load2.dismissAll();
        //       // });
        //
        //         load.dismissAll();
        //     }
        //   },{
        //     text:'Cancel',
        //     handler:()=>{
        //       load.dismissAll();
        //       this.showMessage('Cancelled');
        //     }
        //   }
        //   ]
        // }).present();
        load.dismissAll();
      }).catch(err=>{
        load.dismissAll();
      });
    }))
  }
}
