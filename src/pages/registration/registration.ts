import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the RegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  courses: any[] = [];
  user = new User();
  private frm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpClient, private frmBuilder: FormBuilder, private storage: Storage, private loading: LoadingController, private alertCtrl: AlertController) {
    this.frm = frmBuilder.group({
      fname: ['', [Validators.required, Validators.pattern(`[A-Za-z\\s]{2,}`)]],
      mname: ['', [Validators.pattern(`[A-Za-z\\s]{2,}`)]],
      lname: ['', [Validators.required, Validators.pattern(`[A-Za-z\\s]{2,}`)]],
      email: ['', [Validators.required, Validators.pattern(`[a-z]+@([students\\.]+)?national(-u\\.edu\\.ph)?`)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')]],
      id_num : ['', [Validators.required, Validators.minLength(10)]],
      user_type : ['', [Validators.required]],
      course_id: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() : void {
    this.http.get('http://nuls.x10host.com/select-courses-api.php').subscribe((data: any[]) => {
      data.map((course: any) => {
        this.courses.push(course);
      });
    });
  }

  registerUser() {
    let load = this.loading.create({
      content: 'Registration on process'
    });
    load.present();
    let data = this.frm.value;
    this.user.fname = data.fname;
    this.user.lname = data.lname;
    this.user.mname = data.mname;
    this.user.username = data.email;
    this.user.password = data.password;
    this.user.user_type = data.user_type;
    this.user.id_num = data.id_num;
    this.user.course_id = data.course_id;

    let postData = new FormData();
    postData.append('user', JSON.stringify(this.user));
    this.http.post('http://nuls.x10host.com/register-mobile.php', postData).subscribe((userId) => {
      if(userId !== "failed" && userId !== "taken") {
        this.alertCtrl.create({
          title: 'Successfully Registered',
          buttons: ['Ok']
        }).present();
        this.storage.set('id', userId);
        this.storage.set('user_type', 'user');
        this.storage.set('type', this.user.user_type);
        this.navCtrl.setRoot(TabsPage);
      }
      else if (userId === "taken") {
        this.alertCtrl.create({
          title: 'Email or ID # already registered',
          buttons: ['Ok']
        }).present();
      } else if (userId === "failed") {
        this.alertCtrl.create({
          title: 'Registration Failed',
          buttons: ['Ok']
        }).present();
      }
      load.dismissAll();
    });
  }



}
export class User {
  id?: number;
  fname: string;
  lname: string;
  mname: string;
  id_num: string;
  username: string;
  password: string;
  user_type: string;
  course_id: number;
}
