import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the AddTopicPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-topic',
  templateUrl: 'add-topic.html',
})
export class AddTopicPage {

  topics: string;
  barcode: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpClient, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    this.topics = this.navParams.get('topics');
    this.topics = this.topics.trim();
    this.barcode = this.navParams.get('barcode')
  }

  onSave(): void {
    let postData = new FormData();
          postData.append('barcode', this.barcode);
          postData.append('topic', this.topics);
          this.http.post("http://nuls.x10host.com/add_topic.php", postData).subscribe((res : any) => {
            if (res == '1') {
              this.toastCtrl.create({
                message: 'Added Topics Successfully',
                duration: 2000
              }).present();
              this.navCtrl.pop();
              return;
            }
            this.toastCtrl.create({
              message: 'Adding topics failed, please try again',
              duration: 2000
            }).present();

          });
  }

}
