import { Component } from '@angular/core';
import {AlertController, App, IonicPage, NavController, NavParams,LoadingController} from 'ionic-angular';
import {BarcodeScanner, BarcodeScannerOptions} from "@ionic-native/barcode-scanner";
import {LoginPage} from "../login/login";
import {Storage} from "@ionic/storage";
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})

export class NotificationPage {

  options:BarcodeScannerOptions={};
  bookInfo:Book[]=[];
  tempDate:Date;
  newDate:Date= new Date();
  id:string;
  type: string;
  constructor(private load:LoadingController,private app:App,public navCtrl: NavController, public navParams: NavParams,private Bscanner:BarcodeScanner,private alertCtrl:AlertController,private storage:Storage,private http:HttpClient) {
  }

  ionViewDidLoad() {
    this.storage.get('type').then((data) => {
      this.type = data;
    })
  }
  LoadData():void{
    let spin = this.load.create({
      content:'Please Wait'
    });
    this.bookInfo=[];
    spin.present();
    this.storage.get('id').then(data=>{
      this.id=data;
      let postData = new FormData();
      postData.append('user_id',this.id);
      this.http.post("http://nuls.x10host.com/get_borrowed_books.php",postData).subscribe((res : any)=>{
        console.log(res);
        res.forEach(obj=>{
          this.tempDate = new Date(obj.date_borrowed);
          this.newDate = this.tempDate;
          let b = new Book();
          let d:Date = new Date();
          if(this.type == 'Student') {
            this.newDate.setDate(this.newDate.getDate() + 3);
          } else  {
            this.newDate.setDate(this.newDate.getDate() + 30);
          }
          if(this.newDate.toDateString() == d.toDateString()){
            b.status="Not Yet Overdue";
          }
          else if(this.newDate > d){
            b.status="Not Yet Overdue";
          }
          else{
            b.status="Overdue";
          }
          if (obj.is_approved !== '1') {
            b.status = 'Reserved';
          }

          b.title = obj.title;
          b.is_approved = obj.is_approved;
          b.barcode = obj.barcode;
          b.return_date = moment(this.newDate).format('ll');
          this.bookInfo.push(b);
        },error=>{
          spin.dismiss();
          this.showMessage('Internet Connectivity Problem');
        });
        spin.dismiss();
      },err=>{
        spin.dismiss();
        this.showMessage('Internet Connectivity Problem');
      });

    },error=>{
      spin.dismiss();
      this.showMessage('Internal Problem');
    }),error=>{
      spin.dismissAll();
      this.showMessage('Internal Problem');
    }
  }
  swipe(event):void{
    if(event.direction===4){
      this.navCtrl.parent.select(0);
    }
  }
  showMessage(message:string){
    this.alertCtrl.create({
      title:'Message',
      subTitle:message,
      buttons:['ok']
    }).present();
  }

  Logout ():void{
    let alert = this.alertCtrl.create({
      title:'Logout',
      subTitle:'Do you really want to Logout?',
      buttons:[{
        text:'Yes',
        handler:()=>{
          this.storage.remove('user_type');
          this.storage.remove('id');
          this.app.getRootNav().setRoot(LoginPage);

        }
      },{
        text:'No',
        role:'cancel'
      }]
    });
    alert.present();
  }

  ionViewWillEnter(){
    this.LoadData();
  }

}
export class Book{
  public title:string;
  public return_date:string;
  public status:string;
  public barcode:string;
  public is_approved: any;
}

