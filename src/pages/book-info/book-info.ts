import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import * as moment from 'moment';
import {Storage} from "@ionic/storage";
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the BookInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-book-info',
  templateUrl: 'book-info.html',
})
export class BookInfoPage {
  fromWhere:string;
  bookTitle:string;
  author:string;
  edition:string;
  barcode:string;
  book_id:string;
  section: string;
  copyright_date: string;
  availability:string;
  user_id:string;

  attendance = false;

  av: string;
  type: string;
  date:Date = new Date();
  returnDate:Date = new Date();
  constructor(public navCtrl: NavController, public navParams: NavParams,private  alertCtrl:AlertController,private http:HttpClient,private storage:Storage,private loadingCtrl: LoadingController) {
  this.fromWhere = this.navParams.get('from');
  this.bookTitle = this.navParams.get('book_title');
  this.author = this.navParams.get('author');
  this.edition = this.navParams.get('edition');
  this.barcode = this.navParams.get('barcode');
  this.book_id = this.navParams.get('book_id');
  this.section = this.navParams.get('section');
  if (typeof this.section === 'undefined') {
    this.section = 'Not yet cataloged';
  }
  this.copyright_date = this.navParams.get('copyright_date');
    this.storage.get('id').then(data=>{
      this.user_id = data;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookInfoPage');
    let postData = new  FormData();
    postData.append('book_id',this.book_id);
    this.http.post("http://nuls.x10host.com/check-availability-api.php",postData).subscribe((res: any)=>{
      this.availability = res;
      if(this.availability === '1') {
        this.av = 'Available'
      } else if (this.availability === '0') {
        this.av = 'Unknown Availability';
      }
      else {
        this. av = `Might be available on ${moment(res).format('ll')}`;
          if (this.av === 'Might be available on Invalid date') {
            this.av = 'Please wait until the material is  cataloged';
          }
      }
    });
    this.storage.get('type').then((data) => {
      this.type = data;
    });
    this.storage.get('attendance').then((data) => {
      this.attendance = moment(data).format('ll') === moment().format('ll');
      console.log(data);
      console.log(this.attendance);
    })
  }
  showBorrowed(message:string):void{
    let alert = this.alertCtrl.create({
      title:'Message:',
      subTitle:message,
      buttons:['Ok']
    });
    alert.present();
  }
  BorrowBook():void{
    let postData = new FormData();
    postData.append('barcode',this.barcode);
    postData.append('user_id',this.user_id);
    let loading = this.loadingCtrl.create({
      content:'Please wait'
    });
    loading.present();
    this.http.post("http://nuls.x10host.com/borrow_book_user.php",postData).subscribe((res: any)=>{
      if(res == 1){
        if(this.type == "Student") {
          this.returnDate.setDate(this.date.getDate() + 3);
        } else {
          this.returnDate.setDate(this.date.getDate() + 30);
        }
        this.showBorrowed("Book Reserved, please proceed to the counter/circulation librarian");
        this.navCtrl.pop();
        loading.dismiss();
      }
      else if (res == 2){
        this.showBorrowed('Cannot borrow more that 4 books');
        loading.dismiss();
        this.navCtrl.pop();
      }
      else {
        this.showBorrowed('Cannot Reserve book, might be borrowed by someone else or marked as missing');
        loading.dismiss();
        this.navCtrl.pop();
      }
    },error=>{
      this.showBorrowed('Internet Connection Problem');
    });
  }
  Borrow(){
    let alert = this.alertCtrl.create({
      title:'Confirmation',
      subTitle:'Are you sure you want to borrow this book?',
      buttons:[{
        text:'Yes',
        handler:()=>{
         this.BorrowBook();
        }
      },{
        text:'No',
        handler:()=>{

        }
      }]
    });
    alert.present();
  }
}
