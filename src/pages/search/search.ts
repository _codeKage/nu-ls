import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import {BookInfoPage} from "../book-info/book-info";
import { HttpClient } from '@angular/common/http';
import * as ordinal from 'ordinal';

import * as moment from 'moment';
/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  books:any[] = [];
  keyword:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,private http:HttpClient, private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }
  openBookInfo(book_id:string,book_title:string,author:string,edition:string, copyrigh_date: string, section: string){
    this.navCtrl.push(BookInfoPage,{
      'from':'1',
      'book_id':book_id,
      'book_title':book_title,
      'author':author,
      'edition':edition,
      'copyright_date': copyrigh_date,
      'section': section
    });
  }
  search(): void {
    let postData = new FormData();
    let load = this.loadingCtrl.create({
      content: 'Loading'
    });
    this.books = [];
    load.present();
    postData.append('keyword',this.keyword);
    this.http.post("http://nuls.x10host.com/search-api.php",postData).subscribe((res: any[])=>{
       res.map((item: any) => {
        item.copyright_date = moment(item.copyright_date).format('YYYY');
        item.edition = item.edition;
        this.books.push(item);
        console.log(item);
      });
      load.dismissAll();
    },error=>{
      console.log(error);
      load.dismissAll();
    });
    console.log('test');
  }
}
