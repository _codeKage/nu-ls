import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams,LoadingController} from 'ionic-angular';
import {BarcodeScanner, BarcodeScannerOptions} from "@ionic-native/barcode-scanner";
import {AdminBookInfoPage} from "../admin-book-info/admin-book-info";
import {LoginPage} from "../login/login";
import {Storage} from "@ionic/storage";
import { HttpClient } from '@angular/common/http';

import * as ordinal from 'ordinal';
/**
 * Generated class for the AdminTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-tabs',
  templateUrl: 'admin-tabs.html',
})
export class AdminTabsPage {
  options:BarcodeScannerOptions;
  id: string;
  constructor(private ld:LoadingController,private http:HttpClient,public navCtrl: NavController, public navParams: NavParams,private Bscanner:BarcodeScanner,private alertCtrl:AlertController,private storage:Storage) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
    this.storage.get('id').then((id: string) => {
      this.id = id;
      console.log(this.id);
    })
  }

  async openScanner(){
  let loading = this.ld.create({
    content:'Please Wait'
  });
    this.options = {
      prompt:"Scan Barcode"
    };
    this.Bscanner.scan(this.options).then(barcodeData=>{
      if(barcodeData.cancelled) {
        this.alertCtrl.create({
          subTitle: 'Cancelled',
          buttons: ['ok']
        }).present();
        return;
      }
     let postData = new FormData();
     postData.append('barcode',barcodeData.text);
     if(barcodeData.text.trim() !== "") {
       loading.present();
       this.http.post("http://nuls.x10host.com/user-borrow-search.php", postData).subscribe((res : any) => {
         if (res.length == 0) {
           this.showMessage('Book Barcode does not match any in our library');
           loading.dismissAll();
         } else {
           loading.dismissAll();
           this.navCtrl.push(AdminBookInfoPage, {
             'barcode': barcodeData.text,
             'book_title': res[0].title,
             'author': res[0].author,
             'edition': ordinal(+res[0].edition),
           });

         }

       }, error => {
         loading.dismissAll();
         this.showMessage('Internet Connection error');

       })
     }
    });

  }
  async Inventory (){
    this.options = {
      prompt:"Scan Barcode"
    };
    this.Bscanner.scan(this.options).then(barcodeData=>{
      if(barcodeData.cancelled) {
        this.alertCtrl.create({
          subTitle: 'Cancelled',
          buttons: ['ok']
        }).present();
        return;
      }
      if(barcodeData.text.trim() !== ""){
        this.InventoryUpdate(barcodeData.text, this.id);
        this.Inventory();
      }
    }).catch(() => {
    });
  }

  InventoryUpdate(barcode:string, id: string): void {
    let postData = new FormData();
      postData.append('barcode',barcode);
      postData.append('userId', id);
      this.http.post("http://nuls.x10host.com/inventory_check.php",postData).subscribe((data : any)=>{
      },error=>{
        this.showMessage('Internet Connection error');
      });
  }

  InventoryPrompt(){
    this.Inventory();
  }

  Logout():void{
    let alert = this.alertCtrl.create({
      title:'Logout',
      subTitle:'Do you really want to Logout?',
      buttons:[{
        text:'Yes',
        handler:()=>{
          this.storage.remove('id');
          this.storage.remove('user_type');
          this.navCtrl.setRoot(LoginPage);
        }
      },{
        text:'No',
        role:'cancel'
      }]
    });
    alert.present();
  }
  showMessage(message:string):void{
    this.alertCtrl.create({
      title:'Message',
      subTitle:message,
      buttons:['ok']
    }).present();
  }

  checkBookStatus(): void {
    this.Bscanner.scan().then((barcodeData) => {
      if(barcodeData.cancelled) {
        this.alertCtrl.create({
          subTitle: 'Cancelled',
          buttons: ['ok']
        }).present();
        return;
      }
      let postData = new FormData();
      postData.append('barcode', barcodeData.text);
      if(barcodeData.text.trim() !== "") {
        this.http.post("http://nuls.x10host.com/borrow-checker-api.php", postData).subscribe((res: any) => {
          if (res === null) {
            this.alertCtrl.create({
              subTitle: 'This book is not marked as borrowed',
              buttons: ['ok']
            }).present();
            return;
          }
          this.alertCtrl.create({
            title: 'Book is borrowed',
            subTitle: 'Borrower: ' + res.user_fname + ' ' + res.user_lname + '<br>' + 'ID Number: ' + res.id_number + '<br>' +
              'Date Borrowed: <br>  ' + res.date_borrowed,
            buttons: ['ok']
          }).present();
        });
      }
    });
  }

}
