import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {Storage} from "@ionic/storage";
import {AdminTabsPage} from "../pages/admin-tabs/admin-tabs";
import {TabsPage} from "../pages/tabs/tabs";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {BackgroundMode} from "@ionic-native/background-mode";
import { HttpClient } from '@angular/common/http';
import set = Reflect.set;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  ctr: number = 0;
  type: string;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,storage:Storage,lNotif:LocalNotifications,private bm:BackgroundMode,private http:HttpClient) {
    storage.get('type').then((data: string) => {
      this.type = data;
    });
    bm.setDefaults({
      title:'NU-LS',
      text:'Running',
      hidden: true,
      silent: true
    });
    storage.get('user_type').then(data=>{
      if(data=="librarian"){
        this.rootPage=AdminTabsPage;
        console.log(data);

      }
      else if(data=="user"){
        this.rootPage=TabsPage;
        storage.get('id').then(()=>{
          this.GetNotifications(lNotif,http,storage);
        });
      }
      else{
        this.rootPage=LoginPage;
      }

      platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        bm.enable();
        bm.on('activate').subscribe(() => {
          setInterval(() => {
            storage.get('id').then(()=>{
              this.GetNotifications(lNotif,http,storage);
            });
            console.log('Here');
          }, 3600000 );
        });
        statusBar.styleDefault();
        splashScreen.hide();
      });

    });
  }
  GetData(lNotif:LocalNotifications,http:HttpClient,storage:Storage):void{
    storage.get('id').then(data=>{
      let postData = new FormData();
      postData.append('user_id',data.toString());
      this.http.post("http://nuls.x10host.com/ForNotification.php",postData).subscribe((res: any[])=>{
        let x:string="";
        res.forEach(data=>{
          let returnDate:Date = new Date(data.date_borrowed);
          let newDate:Date = new Date();
          if (this.type === 'Student') {
            returnDate.setDate(returnDate.getDate() + 3);
          } else {
            returnDate.setDate(returnDate.getDate() + 30);
          }
          if(returnDate.toDateString()==newDate.toDateString()){
            x+=data.title+" is only until today, please return it within this day \n";
          }
          else if(newDate < returnDate){
            x+="Please return "+data.title+" until "+returnDate.toDateString()+"\n";
          }
          else {
            x+=data.title+" Overdue \n";
          }
          lNotif.schedule({
            id:data.index,
            title:'Book Status',
            text:x,
            icon:'file://assets/img/icon.png',
            smallIcon:'file://assets/img/icon.png'
          });

        });

      },err=>{

      });
    },err=>{

    });

  }
  GetNotifications(lNotif:LocalNotifications,http:HttpClient,storage:Storage):void {
              this.GetData(lNotif,http,storage);
              this.ctr = 0;
  }
}
